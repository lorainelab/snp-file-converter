package org.lorainelab.igb.snp.convert.action;

import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.lorainelab.igb.menu.api.model.MenuBarParentMenu;
import org.lorainelab.igb.menu.api.model.MenuItem;
import org.lorainelab.igb.snp.convert.ui.SnpConverterFrame;
import org.lorainelab.igb.menu.api.MenuBarEntryProvider;
import org.lorainelab.igb.menu.api.model.MenuIcon;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dcnorris
 */
@Component(immediate = true)
public class SnpConverterAction implements MenuBarEntryProvider {

    private static final Logger logger = LoggerFactory.getLogger(SnpConverterAction.class);
    private static final int MENU_WEIGHT = 35;
    private SnpConverterFrame snpConverterFrame;

    public static final String SNPCONVERTER_ICON = "images/snp-converter-logo.png";

    @Reference
    public void setSnpConverterFrame(SnpConverterFrame snpConverterFrame) {
        this.snpConverterFrame = snpConverterFrame;
    }

    @Override
    public Optional<List<MenuItem>> getMenuItems() {
        MenuItem menuItem = new MenuItem("SNP File Converter", (Void t) -> {
            snpConverterFrame.setVisible(true);
            return t;
        });
        try (InputStream resourceAsStream = SnpConverterAction.class.getClassLoader().getResourceAsStream(SNPCONVERTER_ICON) ) {
            menuItem.setMenuIcon(new MenuIcon(resourceAsStream));
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        menuItem.setWeight(MENU_WEIGHT);
        return Optional.of(Arrays.asList(menuItem));
    }

    @Override
    public MenuBarParentMenu getMenuExtensionParent() {
        return MenuBarParentMenu.TOOLS;
    }
}
